import {
  Container,
  CssBaseline,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import React from 'react';
import { renderRoutes } from 'react-router-config';
import { TopMenu } from '../components';
import { ArrowBack as ArrowBackIosIcon } from '@material-ui/icons';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '100vw',
    minHeight: '100vh',
    background: '#3D3D3D',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  topMenuContainer: {
    width: theme.spacing(20),
    height: theme.spacing(10),
    position: 'fixed',
    top: 0,
    right: 0,
    zIndex: 1,
  },
  back: {
    display: 'flex',
    alignItems: 'center',
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 2,
  },
}));

const SettingsLayout = ({ route }) => {
  const classes = useStyles();
  const history = useHistory();

  const handleBack = () => {
    history.goBack();
  };

  return (
    <Container className={classes.root}>
      <CssBaseline />
      <div className={classes.topMenuContainer}>
        <TopMenu />
      </div>
      <div className={classes.back}>
        <IconButton>
          <ArrowBackIosIcon onClick={handleBack} />
        </IconButton>
        <Typography variant="h5">
          {history.location.pathname === '/settings' ? 'Settings' : 'Commands'}
        </Typography>
      </div>
      <div>{renderRoutes(route?.routes)}</div>
    </Container>
  );
};

export default SettingsLayout;
