import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import './index.css';
import { getAlbumInfoRequest, getTrackInfoRequest } from '../../../../ducks';

const PlayCardItem = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { description, imgUrl, title, artist, isArtist, isAlbum, isTrack } =
    props;

  const handleTitleClick = () => {
    if (isAlbum) {
      dispatch(getAlbumInfoRequest({ name: title, artist }, history));
    } else {
      dispatch(getTrackInfoRequest({ name: title, artist }));
    }
  };

  return (
    <div className="fade-in-bottom">
      <div className="CategoryContainer" onClick={handleTitleClick}>
        <img className="cl" src={imgUrl} alt={title}></img>
        <Link onClick={handleTitleClick} href="#" variant="h6">
          {title}
          <br></br>
        </Link>
        {description && (
          <Link href="#" variant="body2">
            {description}
          </Link>
        )}
        <div className={!isAlbum ? 'playContent' : 'playHover'}>
          <PlayArrowIcon
            onClick={handleTitleClick}
            className="playClic"
            ></PlayArrowIcon>
        </div>
      </div>
    </div>
  );
};

export default PlayCardItem;
