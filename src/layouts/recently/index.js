import React from 'react';
import PlayCardItem from '../playCard';
import './index.css';

const PlayComponent = (props) => {
  const { description, imgUrl, title } = props;

  return (
    <div className="CategoryItens">
      <PlayCardItem
        description={description}
        imgUrl={imgUrl}
        title={title}
      ></PlayCardItem>

      <PlayCardItem
        description={description}
        imgUrl={imgUrl}
        title={title}
      ></PlayCardItem>
    </div>
  );
};

export default PlayComponent;
