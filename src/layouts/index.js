export { default as GeneralLayout } from './general';
export { default as SettingUpLayout } from './settingUp';
export { default as PlayerLayout } from './player';
export { default as PlayListLayout } from './components/playList';
export { default as SettingsLayout } from './settings';
