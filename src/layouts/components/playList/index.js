import {
  makeStyles,
  withStyles,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@material-ui/core';
import * as React from 'react';

import Paper from '@material-ui/core/Paper';
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { useDispatch, useSelector } from 'react-redux';
import { getTrackInfoRequest } from '../../../ducks';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  root: {
    border: 'none',
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    border: '0',
  },
}))(TableRow);

function song(name, artist, icon, album) {
  return { name, artist, icon, album };
}

const useStyles2 = makeStyles({
  table: {
    minWidth: 500,
  },

  font: {
    backgroundColor: '#565454',
  },
});

const PlayListLayout = (props) => {
  const classes = useStyles2();
  const [rows, setRows] = React.useState([
    song('No Surprises', 'RadioHead', true, '3:17'),
    song('Lordly - Instrumental', 'Feder', false, '4:44'),
    song('RLNDT', 'Bad Bunnt ', true, '3:39'),
  ]);
  const album = useSelector((state) => state.album.album);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (props.populate && album.tracks != null) {
      setRows(album.tracks);
    }
  }, [album]);

  const toggleFavorite = (favorite) => {
    favorite = !favorite;
  };

  const playSong = (name, artist) => {
    dispatch(getTrackInfoRequest({ name, artist }));
  };

  return (
    <TableContainer className={classes.font} component={Paper}>
      <Table className={classes.table} aria-label="custom pagination table">
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell style={{ width: 16 }}>
                <IconButton onClick={() => playSong(row.name, row.artist)}>
                  <PlayCircleFilledIcon />
                </IconButton>
              </StyledTableCell>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <StyledTableCell style={{ width: 160 }} align="left">
                {row.artist}
              </StyledTableCell>
              <StyledTableCell style={{ width: 360 }} align="right">
                <IconButton onClick={toggleFavorite(row.icon)}>
                  {row.icon ? (
                    <FavoriteIcon fontSize="inherit" color="primary" />
                  ) : (
                    <FavoriteBorderIcon fontSize="inherit" color="primary" />
                  )}
                </IconButton>
              </StyledTableCell>
              <StyledTableCell style={{ width: 20 }} align="right">
                {row.album}
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default PlayListLayout;
