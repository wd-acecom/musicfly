import { useEffect, useState } from 'react';
import {
  Menu,
  MenuItem,
  Avatar,
  makeStyles,
  withStyles,
  Typography,
  IconButton,
  ListItemIcon,
} from '@material-ui/core';
import {
  Menu as MenuIcon,
  Settings as SettingsIcon,
  ExitToApp as LogoutIcon,
} from '@material-ui/icons';
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
  avatar: {
    width: theme.spacing(9),
    height: theme.spacing(9),
    position: 'relative',
  },
  menuContainer: {
    width: theme.spacing(20),
    height: theme.spacing(9),
    backgroundColor: theme.palette.secondary.main,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopLeftRadius: theme.spacing(5),
    borderBottomLeftRadius: theme.spacing(5),
  },
}));

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #4E2355',
    backgroundColor: '#4E2355',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
    backgroundColor: '#4E2355',
  },
}))(MenuItem);

const TopMenu = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const firebase = useFirebase();
  const history = useHistory();
  const auth = useSelector((state) => state.firebase.auth);

  useEffect(() => {
    if (isLoaded(auth) && isEmpty(auth)) {
      history.push('/');
    }
  }, [auth, history]);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget.parentElement);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    firebase.logout();
  };

  const toSettings = () => {
    history.push('/settings');
  };

  return (
    <div>
      <div className={classes.menuContainer}>
        <Avatar className={classes.avatar}>DA</Avatar>
        <IconButton
          aria-controls="customized-menu"
          aria-haspopup="true"
          variant="contained"
          color="primary"
          onClick={handleClick}
        >
          <MenuIcon />
        </IconButton>
      </div>

      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenuItem onClick={toSettings}>
          <ListItemIcon>
            <SettingsIcon fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Settings</Typography>
        </StyledMenuItem>
        <StyledMenuItem onClick={handleLogout}>
          <ListItemIcon>
            <LogoutIcon fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Logout</Typography>
        </StyledMenuItem>
      </StyledMenu>
    </div>
  );
};

export default TopMenu;
