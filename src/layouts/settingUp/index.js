import { Container, CssBaseline, makeStyles } from '@material-ui/core';
import React from 'react';
import { renderRoutes } from 'react-router-config';
import { TopMenu } from '../components';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '100vw',
    minHeight: '100vh',
    background: '#3D3D3D',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  topMenuContainer: {
    width: theme.spacing(20),
    height: theme.spacing(10),
    position: 'fixed',
    top: 0,
    right: 0,
    zIndex: 1,
  },
}));

const SettingUpLayout = ({ route }) => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <CssBaseline />
      <div className={classes.topMenuContainer}>
        <TopMenu />
      </div>
      <div>{renderRoutes(route?.routes)}</div>
    </Container>
  );
};

export default SettingUpLayout;
