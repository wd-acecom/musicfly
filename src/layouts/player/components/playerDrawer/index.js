import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
  Search as SearchIcon,
  Favorite as FavoriteIcon,
  History as HistoryIcon,
} from '@material-ui/icons';
import MusicflyCoreImage from '../../../../assets/musicfly-core.svg';
import { ListSubheader } from '@material-ui/core';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: theme.spacing(40),
  },
  drawerPaper: {
    width: theme.spacing(30),
    backgroundColor: '#3D3D3D',
    border: '1px solid #474747',
    maxHeight: `calc(100vh - ${theme.spacing(10)}px)`,
  },
  divider: {
    backgroundColor: '#474747',
  },
  toolbar: {
    ...theme.mixins.toolbar,
    display: 'flex',
    width: '100%',
    height: 'auto',
  },
  coreImage: { 
    objectFit: 'none',
  },
}));

export default function MiniDrawer() {
  const classes = useStyles();
  const history = useHistory();

  const toDiscover = () => {
    history.push('/player/discover');
  };

  const toRecentlyPlayed = () => {
    history.push('/player/recentlyPlayed');
  };

  const toPlayList = () => {
    history.push('/player/playlists');
  };
  const toFavorite = () => {
    history.push('/player/favorites');
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
      anchor="left"
    >
      <div className={classes.toolbar}>
        <img
          className={classes.coreImage}
          src={MusicflyCoreImage}
          alt="musicfly logo"
        />
      </div>
      <Divider className={classes.divider} />
      <List>
        <ListItem button onClick={toDiscover}>
          <ListItemIcon>
            <SearchIcon />
          </ListItemIcon>
          <ListItemText primary={'Discover'} />
        </ListItem>
        <Divider className={classes.divider} />
        <ListSubheader>Library</ListSubheader>
        <ListItem button onClick={toRecentlyPlayed}>
          <ListItemIcon>
            <HistoryIcon />
          </ListItemIcon>
          <ListItemText primary={'Recently played'} />
        </ListItem>
        <ListItem button onClick={toFavorite}>
          <ListItemIcon>
            <FavoriteIcon />
          </ListItemIcon>
          <ListItemText primary={'Favorites'} />
        </ListItem>
        <Divider className={classes.divider} />
        <ListSubheader>Playlists</ListSubheader>
        <ListItem button onClick={toPlayList}>
          <ListItemText primary={'#CatchMyHeadBanging'} />
        </ListItem>
        <ListItem button>
          <ListItemText primary={'#Christmas'} />
        </ListItem>
      </List>
    </Drawer>
  );
}
