import {
  makeStyles,
  Typography,
  Link,
  IconButton,
  Slider,
} from '@material-ui/core';
import albumImage from '../../../../assets/album_example.png';
import {
  Favorite as FavoriteIcon,
  FavoriteBorder as FavoriteBorderIcon,
  PlayArrow as PlayIcon,
  Stop as StopIcon,
  SkipPrevious as PreviousIcon,
  SkipNext as NextIcon,
  Shuffle as ShuffleIcon,
  Pause as PauseIcon,
  VolumeDown as VolumeDownIcon,
  VolumeMute as VolumeMuteIcon,
  VolumeOff as VolumeOffIcon,
  VolumeUp as VolumeUpIcon,
  Fullscreen as FullScreenIcon,
  QueueMusic as QueueMusicIcon,
} from '@material-ui/icons';
import { useSelector } from 'react-redux';
import { useState, useEffect, useRef } from 'react';
import { enrrollCommants } from '../../../../api';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: theme.spacing(10),
    backgroundColor: theme.palette.secondary.main,
    display: 'flex',
    justifyContent: 'space-between',
  },
  songInfoContainer: {
    width: theme.spacing(40),
    display: 'flex',
  },
  songImage: {
    width: theme.spacing(10),
  },
  favoriteActionContainer: {
    width: theme.spacing(8),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  songInfo: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: theme.spacing(1),
  },
  controlsContainer: {
    width: theme.spacing(70),
  },
  actionsContainer: {
    height: theme.spacing(5),
    display: 'flex',
    justifyContent: 'center',
  },
  timelineContainer: {
    height: theme.spacing(5),
    display: 'flex',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  optionsController: {
    width: theme.spacing(40),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  volumeContainer: {
    width: theme.spacing(20),
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  video: {
    // display: 'none',
    position: 'fixed',
    top: 200,
    left: 300,
  },
}));

const toMSFormat = (seconds) => {
  let _minutes = Math.floor(seconds / 60);
  let _seconds = seconds - _minutes * 60;

  return `${_minutes}:${_seconds.toString().padStart(2, '0')}`;
};

const PlayerBar = () => {
  const classes = useStyles();
  const [favorite, setFavorite] = useState(false);
  const [shuffle, setShuffle] = useState(false);
  const [playing, setPlaying] = useState(false);
  const [value, setValue] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [totalTime, setTotalTime] = useState(0);
  const [volumeLevel, setVolumeLevel] = useState(100);
  const [volume, setVolume] = useState(true);
  const [playerRef, setPlayerRef] = useState(null);
  const track = useSelector((state) => state.player.current);

  const keyVoice = useSelector((state) => state.settings.keyVoice);

  const toggleFavorite = () => {
    setFavorite(!favorite);
  };

  const toggleShuffle = () => {
    setShuffle(!shuffle);
  };

  const handleSliderChange = (_, newValue) => {
    setValue(newValue);
    playerRef.seekTo(newValue);
  };

  const handleVolumeLevelChange = (_, newVolume) => {
    if (playerRef != null) {
      playerRef.setVolume(volumeLevel);
    }
    setVolumeLevel(newVolume);
  };

  const handleVolume = () => {
    if (playerRef != null) {
      volume ? playerRef.mute() : playerRef.unMute();
    }
    setVolume(!volume);
  };

  const updateInterval = useRef(null);

  const loadVideo = () => {
    console.log(track.videoUrl);
    let prev = document.getElementById('ytplayer');
    if (prev) {
      prev.remove();
    }
    let g = document.createElement('div');
    g.setAttribute('id', 'ytplayer');
    let first = document.getElementById('song-info');
    first.parentNode.insertBefore(g, first);

    setPlayerRef(null);
    setCurrentTime(0);
    setValue(0);
    setPlaying(false);

    window.YT.ready(() => {
      new window.YT.Player('ytplayer', {
        height: '360',
        width: '640',
        videoId: track.videoUrl.replace('https://www.youtube.com/watch?v=', ''),
        events: {
          onReady: (event) => {
            setPlayerRef(event.target);
          },
        },
      });
    });
  };
  const botPlay = () => {
    if (!playing) {
      playerRef.playVideo();
      setPlaying(!playing);
    }
  };

  const botPause = () => {
    if (playing) {
      playerRef.pauseVideo();
      setPlaying(!playing);
    }
  };

  const botLike = () => {
    toggleFavorite();
  };

  const botMute = () => {
    handleVolume();
  };
  enrrollCommants([
    {
      indexes: [`${keyVoice} play`],
      action: botPlay,
    },
    {
      indexes: [`${keyVoice} pause`],
      action: botPause,
    },
    {
      indexes: [`${keyVoice} stop`],
      action: botPause,
    },
    {
      indexes: [`${keyVoice} like`],
      action: botLike,
    },
    {
      indexes: [`${keyVoice} mute`],
      action: botMute,
    },
  ]);

  useEffect(() => {
    if (playerRef) {
      setTotalTime(playerRef.getDuration());
      updateInterval.current = setInterval(() => {
        setCurrentTime(Math.floor(playerRef.getCurrentTime()));
        setValue(playerRef.getCurrentTime());
        setVolumeLevel(playerRef.getVolume());
      }, 100);
    } else {
      if (updateInterval.current) {
        clearInterval(updateInterval.current);
      }
    }
  }, [playerRef]);

  useEffect(() => {
    if (!window.YT) {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';

      window.onYouTubeIframeAPIReady = loadVideo;

      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    } else {
      loadVideo();
    }
  }, [track]);

  const togglePlaying = () => {
    console.log(playerRef.getDuration());
    if (playing) {
      playerRef.pauseVideo();
    } else {
      playerRef.playVideo();
    }
    setPlaying(!playing);
  };

  return (
    <div className={classes.root}>
      {/* <ReactPlayer className={classes.video} url={track.videoUrl} /> */}
      <div id="ytplayer" className={classes.video}></div>
      <div id="song-info" className={classes.songInfoContainer}>
        <img className={classes.songImage} src={track.image} alt="song cover" />
        <div className={classes.favoriteActionContainer}>
          <IconButton onClick={toggleFavorite}>
            {favorite ? (
              <FavoriteIcon fontSize="inherit" color="primary" />
            ) : (
              <FavoriteBorderIcon fontSize="inherit" color="primary" />
            )}
          </IconButton>
        </div>
        <div className={classes.songInfo}>
          <Typography variant="subtitle2">{track.name}</Typography>
          <Typography>
            <Link href="#" variant="caption text">
              {track.artist}
            </Link>
          </Typography>
        </div>
      </div>
      <div className={classes.controlsContainer}>
        <div className={classes.actionsContainer}>
          <IconButton onClick={toggleShuffle}>
            <ShuffleIcon color={shuffle ? 'primary' : 'inherit'} />
          </IconButton>
          <IconButton>
            <PreviousIcon color="primary" />
          </IconButton>
          <IconButton onClick={togglePlaying}>
            {playing ? (
              <PauseIcon color="primary" />
            ) : (
              <PlayIcon color="primary" />
            )}
          </IconButton>
          <IconButton>
            <NextIcon color="primary" />
          </IconButton>
          <IconButton>
            <StopIcon color="primary" />
          </IconButton>
        </div>
        <div className={classes.timelineContainer}>
          <Typography variant="caption">{toMSFormat(currentTime)}</Typography>
          <Slider
            value={typeof value === 'number' ? value : 0}
            max={totalTime}
            onChange={handleSliderChange}
          />
          <Typography variant="caption">{toMSFormat(totalTime)}</Typography>
        </div>
      </div>
      <div className={classes.optionsController}>
        <IconButton color="primary">
          <QueueMusicIcon />
        </IconButton>
        <div className={classes.volumeContainer}>
          <IconButton onClick={handleVolume} color="primary">
            {volume && volumeLevel === 0 && <VolumeMuteIcon />}
            {volume && volumeLevel !== 0 && volumeLevel < 50 && (
              <VolumeDownIcon />
            )}
            {volume && volumeLevel >= 50 && <VolumeUpIcon />}
            {!volume && <VolumeOffIcon />}
          </IconButton>
          <Slider
            value={volumeLevel}
            onChange={handleVolumeLevelChange}
            aria-labelledby="continuous-slider"
          />
        </div>
        <IconButton color="primary">
          <FullScreenIcon />
        </IconButton>
      </div>
    </div>
  );
};

export default PlayerBar;
