import { Container, CssBaseline, makeStyles } from '@material-ui/core';
import React from 'react';
import { renderRoutes } from 'react-router-config';
import { TopMenu } from '../components';
import { PlayerBar, PlayerDrawer } from './components';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '100vw',
    height: '100vh',
    background: '#3D3D3D',
  },
  topMenuContainer: {
    width: theme.spacing(20),
    height: theme.spacing(10),
    position: 'fixed',
    top: 0,
    right: 0,
    zIndex: 1,
  },
  playerControlsContainer: {
    width: '100vw',
    height: theme.spacing(10),
    position: 'fixed',
    bottom: 0,
    left: 0,
  },
  content: {
    overflow: 'auto',
    position: 'relative',
    width: `calc(100vw - ${theme.spacing(30)}px)`,
    height: `calc(100vh - ${theme.spacing(10)}px)`,
    left: theme.spacing(30),
  },
}));

const PlayerLayout = ({ route }) => {
  const classes = useStyles();

  return (
    <Container className={classes.root} maxWidth={false} disableGutters={true}>
      <CssBaseline />
      <PlayerDrawer />
      <div className={classes.topMenuContainer}>
        <TopMenu />
      </div>
      <div className={classes.content}>{renderRoutes(route?.routes)}</div>
      <div className={classes.playerControlsContainer}>
        <PlayerBar />
      </div>
    </Container>
  );
};

export default PlayerLayout;
