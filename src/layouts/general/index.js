import { Container, CssBaseline, makeStyles } from '@material-ui/core';
import React from 'react';
import { renderRoutes } from 'react-router-config';

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: '100vw',
    minHeight: '100vh',
    background: 'linear-gradient(180deg, #3d3d3d 65.1%, #874180 100%)',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

const GeneralLayout = ({ route }) => {
  const classes = useStyles();

  return (
    <Container className={classes.root}>
      <CssBaseline />
      <div>{renderRoutes(route?.routes)}</div>
    </Container>
  );
};

export default GeneralLayout;
