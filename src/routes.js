import {
  GeneralLayout,
  SettingUpLayout,
  PlayerLayout,
  SettingsLayout,
} from './layouts';
import {
  SignIn,
  SignUp,
  CommandsNaming,
  CommandsTests,
  Discover,
  DiscoverAlbum,
  RecentlyPlayed,
  Playlist,
  Favorite,
  CommandSettings,
  InitialSettings,
} from './views';
import { Redirect } from 'react-router-dom';
import withAuthentication from './hocs/withAuthentication';

const routes = [
  {
    path: '/settingUp',
    component: SettingUpLayout,
    routes: [
      {
        path: '/settingUp',
        exact: true,
        component: () => <Redirect to="/settingUp/commandNaming" />,
      },
      {
        path: '/settingUp/commandNaming',
        exact: true,
        component: withAuthentication(CommandsNaming),
      },
      {
        path: '/settingUp/commandTests',
        exact: true,
        component: withAuthentication(CommandsTests),
      },
    ],
  },
  {
    path: '/player',
    component: PlayerLayout,
    routes: [
      {
        path: '/player',
        exact: true,
        component: () => <Redirect to="/player/discover" />,
      },
      {
        path: '/player/discover',
        exact: true,
        component: withAuthentication(Discover),
      },
      {
        path: '/player/discover/album',
        exact: true,
        component: withAuthentication(DiscoverAlbum),
      },
      {
        path: '/player/recentlyPlayed',
        exact: true,
        component: withAuthentication(RecentlyPlayed),
      },

      {
        path: '/player/favorites',
        exact: true,
        component: withAuthentication(Favorite),
      },

      {
        path: '/player/playlists',
        exact: true,
        component: withAuthentication(Playlist),
      },
    ],
  },
  {
    path: '/settings',
    component: SettingsLayout,
    routes: [
      {
        path: '/settings',
        exact: true,
        component: InitialSettings,
      },
      {
        path: '/settings/commands',
        exact: true,
        component: CommandSettings,
      },
    ],
  },
  {
    path: '/',
    component: GeneralLayout,
    routes: [
      {
        path: '/',
        exact: true,
        component: () => <Redirect to="/signIn" />,
      },
      {
        path: '/signIn',
        exact: true,
        component: SignIn,
      },
      {
        path: '/signUp',
        exact: true,
        component: SignUp,
      },
    ],
  },
];

export default routes;
