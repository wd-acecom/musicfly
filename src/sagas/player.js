import { call, put, takeLatest } from 'redux-saga/effects';

import * as ducks from '../ducks';
import * as tools from '../tools';

function* getTrackInfo(action) {
  try {
    const { params } = action.payload;
    const trackInfo = yield call(
      tools.Get,
      `/track/info?name=${params.name}&artist=${params.artist}`
    );
    yield put(ducks.getTrackInfoSuccess(trackInfo));
  } catch (error) {
    console.log(error);
  }
}

export function* getTrackInfoSaga() {
  yield takeLatest(ducks.GET_TRACK_INFO_REQUEST, getTrackInfo);
}
