import { all, fork } from 'redux-saga/effects';

import * as discoverSagas from './discover';
import * as albumSagas from './album';
import * as playerSagas from './player';

function* rootSaga() {
  yield all([
    fork(discoverSagas.getAlbumsSearchSaga),
    fork(discoverSagas.getArtistSearchSaga),
    fork(discoverSagas.getTracksSearchSaga),

    fork(albumSagas.getAlbumInfoSaga),

    fork(playerSagas.getTrackInfoSaga),
  ]);
}

export default rootSaga;
