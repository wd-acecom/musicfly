import { call, put, takeLatest } from 'redux-saga/effects';

import * as ducks from '../ducks';
import * as tools from '../tools';

function* getAlbumInfo(action) {
  try {
    const { params, history } = action.payload;
    const albumInfo = yield call(
      tools.Get,
      `/album/info?name=${params.name}&artist=${params.artist}`
    );
    yield put(ducks.getAlbumInfoSuccess(albumInfo));
    history.push('/player/discover/album');
  } catch (error) {
    console.log(error);
  }
}

export function* getAlbumInfoSaga() {
  yield takeLatest(ducks.GET_ALBUM_INFO_REQUEST, getAlbumInfo);
}
