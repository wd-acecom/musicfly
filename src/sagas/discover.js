import { call, put, takeLatest } from 'redux-saga/effects';

import * as ducks from '../ducks';
import * as tools from '../tools';

function* getAlbumsSearch(action) {
  try {
    const { params } = action.payload;
    const albums = yield call(
      tools.Get,
      `/album/search?value=${params.search}`
    );
    yield put(ducks.getAlbumsSearchSuccess(albums));
  } catch (error) {
    console.log(error);
  }
}

function* getArtistsSearch(action) {
  try {
    const { params } = action.payload;
    const artists = yield call(
      tools.Get,
      `/artist/search?value=${params.search}`
    );
    yield put(ducks.getArtistsSearchSuccess(artists));
  } catch (error) {
    console.log(error);
  }
}

function* getTracksSearch(action) {
  try {
    const { params } = action.payload;
    const tracks = yield call(
      tools.Get,
      `/track/search?value=${params.search}`
    );
    yield put(ducks.getTracksSearchSuccess(tracks));
  } catch (error) {
    console.log(error);
  }
}

export function* getAlbumsSearchSaga() {
  yield takeLatest(ducks.GET_ALBUMS_SEARCH_REQUEST, getAlbumsSearch);
}

export function* getArtistSearchSaga() {
  yield takeLatest(ducks.GET_ARTISTS_SEARCH_REQUEST, getArtistsSearch);
}

export function* getTracksSearchSaga() {
  yield takeLatest(ducks.GET_TRACKS_SEARCH_REQUEST, getTracksSearch);
}
