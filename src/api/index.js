import Artyom from 'artyom.js';

const artyom = new Artyom();

export function enrrollCommants(aux) {
  artyom.emptyCommands();
  aux.map((num) => {
    artyom.addCommands({
      indexes: num.indexes,
      action: num.action,
    });
  });
}

export function startContinuousArtyom() {
  artyom.fatality(); // Detener cualquier instancia previa

  setTimeout(function () {
    // Esperar 250ms para inicializar
    artyom
      .initialize({
        lang: 'en-GB', // Más lenguajes son soportados, lee la documentación
        continuous: true, // Artyom obedecera por siempre
        listen: true, // Iniciar !
        debug: true, // Muestra un informe en la consola
        speed: 1, // Habla normalmente
      })
      .then(function () {
        console.log('Ready to work !');
      });
  }, 250);
}

export function deleteArtyom(val, key) {
  if (val) {
    startContinuousArtyom();
  } else {
    artyom.fatality().then(() => {
      artyom.say(`${key} succesfully stopped !`);
    });
  }
}
