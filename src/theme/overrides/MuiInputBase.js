const MuiInputBase = {
  root: {
    border: '1px',
  },
  input: {
    '&:-webkit-autofill': {
      boxShadow: '0 0 0px 1000px #3d3d3d inset',
      webkitBoxShadow: '0 0 0px 1000px #3d3d3d inset',
      transition: 'background-color 5000s ease-in-out 0s',
    },
    '&:-webkit-autofill:focus': {
      boxShadow: '0 0 0px 1000px #3d3d3d inset',
      webkitBoxShadow: '0 0 0px 1000px #3d3d3d inset',
      transition: 'background-color 5000s ease-in-out 0s',
    },
    '&:-webkit-autofill:hover': {
      boxShadow: '0 0 0px 1000px #3d3d3d inset',
      webkitBoxShadow: '0 0 0px 1000px #3d3d3d inset',
      transition: 'background-color 5000s ease-in-out 0s',
    },
  },
};

export default MuiInputBase;
