import MuiInputBase from './MuiInputBase';
import MuiLink from './MuiLink';
import MuiIconButton from './MuiIconButton';

const overrides = {
  MuiLink,
  MuiInputBase,
  MuiIconButton,
};

export default overrides;
