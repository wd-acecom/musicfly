import { colors } from '@material-ui/core';

const white = '#FFFFFF';
const black = '#000000';

const palette = {
  black,
  white: {
    main: white,
  },
  primary: {
    contrastText: white,
    dark: '#f9e0bf',
    main: '#eb9628',
    light: '#e27918',
  },
  secondary: {
    contrastText: white,
    dark: '#dbc6d9',
    main: '#874180',
    light: '#6a2a63',
  },
  error: {
    contrastText: white,
    dark: colors.red[900],
    main: colors.red[600],
    light: colors.red[400],
  },
  text: {
    primary: '#c4c4c4',
    secondary: '#c4c4c4',
    link: colors.blue[600],
  },
  link: colors.blue[800],
  icon: colors.blueGrey[600],
  background: {
    default: '#3d3d3d',
    paper: 'white',
  },
  divider: colors.grey[200],
};

export default palette;
