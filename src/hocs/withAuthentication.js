import { useSelector } from 'react-redux';
import { isEmpty, isLoaded } from 'react-redux-firebase';
import { Redirect } from 'react-router-dom';

const withAuthentication = (Component) => () => {
  const { auth } = useSelector((state) => state.firebase);

  if (isLoaded(auth) && isEmpty(auth)) {
    return <Redirect to="/" />;
  }

  return <Component />;
};

export default withAuthentication;
