import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import discoverReducer from './discover';
import albumReducer from './album';
import playerReducer from './player';
import settingsReducer from './settings';

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  discover: discoverReducer,
  album: albumReducer,
  player: playerReducer,
  settings: settingsReducer,
});

export default rootReducer;

export * from './discover';
export * from './album';
export * from './player';
export * from './settings';