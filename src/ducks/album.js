export const GET_ALBUM_INFO_REQUEST = 'album/GET_ALBUM_INFO_REQUEST';
export const GET_ALBUM_INFO_SUCCESS = 'album/GET_ALBUM_INFO_SUCCESS';
export const GET_ALBUM_INFO_ERROR = 'album/GET_ALBUM_INFO_ERROR';

const initialState = {
  album: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALBUM_INFO_SUCCESS:
      return {
        ...state,
        album: action.payload.album,
      };
    default:
      return state;
  }
}

export const getAlbumInfoRequest = (params, history) => ({
  type: GET_ALBUM_INFO_REQUEST,
  payload: { params, history },
});

export const getAlbumInfoSuccess = (album) => ({
  type: GET_ALBUM_INFO_SUCCESS,
  payload: { album },
});
