const SET_KEY_VOICE = "SET_KEY_VOICE";
const SET_ON_VOICE = "SET_ON_VOICE";
const inicialState = {
    keyVoice : "musicfly",
    onVoice: true,
};

const reducer = (state=inicialState,action) => {
    switch (action.type) {
        case SET_KEY_VOICE: {
                state = {
                    ...state,
                    keyVoice: action.payload.keyVoice
                }
                return state;
            }
        case SET_ON_VOICE: {
            state = {
                ...state,
                onVoice: action.payload.onVoice
            }
        }   
        default:
            return state;
    }
};

export const setKeyVoice = (keyVoice) => {
    return ({
        type: SET_KEY_VOICE,
        payload: {
            keyVoice
        }
    });
};

export const setOnVoice = (onVoice) => {
    return ({
        type: SET_ON_VOICE,
        payload: {
            onVoice
        }
    });  
}

export default reducer;