export const GET_ALBUMS_SEARCH_REQUEST = 'discover/GET_ALBUMS_SEARCH_REQUEST';
export const GET_ALBUMS_SEARCH_SUCCESS = 'discover/GET_ALBUMS_SEARCH_SUCCESS';
export const GET_ALBUMS_SEARCH_ERROR = 'discover/GET_ALBUMS_SEARCH_ERROR';

export const GET_ARTISTS_SEARCH_REQUEST = 'discover/GET_ARTISTS_SEARCH_REQUEST';
export const GET_ARTISTS_SEARCH_SUCCESS = 'discover/GET_ARTISTS_SEARCH_SUCCESS';
export const GET_ARTISTS_SEARCH_ERROR = 'discover/GET_ARTISTS_SEARCH_ERROR';

export const GET_TRACKS_SEARCH_REQUEST = 'discover/GET_TRACKS_SEARCH_REQUEST';
export const GET_TRACKS_SEARCH_SUCCESS = 'discover/GET_TRACKS_SEARCH_SUCCESS';
export const GET_TRACKS_SEARCH_ERROR = 'discover/GET_TRACKS_SEARCH_ERROR';

const initialState = {
  albums: [],
  artists: [],
  tracks: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALBUMS_SEARCH_SUCCESS:
      return {
        ...state,
        albums: action.payload.albums,
      };
    case GET_ARTISTS_SEARCH_SUCCESS:
      return {
        ...state,
        artists: action.payload.artists,
      };
    case GET_TRACKS_SEARCH_SUCCESS:
      return {
        ...state,
        tracks: action.payload.tracks,
      };
    default:
      return state;
  }
}

export const getAlbumsSearchRequest = (params) => ({
  type: GET_ALBUMS_SEARCH_REQUEST,
  payload: { params },
});

export const getAlbumsSearchSuccess = (albums) => ({
  type: GET_ALBUMS_SEARCH_SUCCESS,
  payload: { albums },
});

export const getArtistsSearchRequest = (params) => ({
  type: GET_ARTISTS_SEARCH_REQUEST,
  payload: { params },
});

export const getArtistsSearchSuccess = (artists) => ({
  type: GET_ARTISTS_SEARCH_SUCCESS,
  payload: { artists },
});

export const getTracksSearchRequest = (params) => ({
  type: GET_TRACKS_SEARCH_REQUEST,
  payload: { params },
});

export const getTracksSearchSuccess = (tracks) => ({
  type: GET_TRACKS_SEARCH_SUCCESS,
  payload: { tracks },
});
