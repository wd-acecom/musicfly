import {
  Button,
  Container,
  makeStyles,
  Switch,
  Typography,
} from '@material-ui/core';
import { useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { setOnVoice } from '../../../ducks';
import {
  deleteArtyom,
} from '../../../api';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  option: {
    display: 'flex',
    margin: theme.spacing(2),
    width: theme.spacing(50),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonRoot: {
    marginTop: theme.spacing(5),
    width: theme.spacing(40),
    height: theme.spacing(5),
    borderRadius: theme.spacing(20),
    background:
      'linear-gradient(90deg, #671A6C 0%, #2787FF 100%, #58A1FC 100%);',
  },
}));

const InitialSettings = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const onVoice = useSelector(state => state.settings.onVoice);
  const keyVoice = useSelector(state => state.settings.keyVoice);

  const toEditCommands = () => {
    history.push('/settings/commands');
  };
  const handleChange = () =>{
    dispatch(setOnVoice(!onVoice));
    deleteArtyom(onVoice,keyVoice)
  };
  return (
    <Container maxWidth="md" className={classes.root}>
      <div className={classes.option}>
        <Typography variant="h6">Run at startup</Typography>
        <Switch color="primary" />
      </div>
      <div className={classes.option}>
        <Typography variant="h6">Minimize to system tray on exit</Typography>
        <Switch color="primary" />
      </div>
      <div className={classes.option}>
        <Typography variant="h6">Use voice</Typography>
        <Switch 
        color="primary" 
        checked={!onVoice}
        onChange={handleChange}
        />
      </div>
      <Button
        onClick={toEditCommands}
        classes={{
          root: classes.buttonRoot,
        }}
      >
        Edit commands
      </Button>
    </Container>
  );
};

export default InitialSettings;
