import {
  Button,
  Container,
  makeStyles,
  Typography,
  IconButton,
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#120000',
    padding: theme.spacing(3),
    width: theme.spacing(80),
    borderRadius: theme.spacing(5),
  },
  option: {
    display: 'flex',
    margin: theme.spacing(2),
    width: theme.spacing(50),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  floatingButton: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    borderRadius: theme.spacing(20),
    backgroundColor: '#EB9628',
    position: 'absolute',
    top: -theme.spacing(2),
    right: theme.spacing(5),
    '&:hover': {
      backgroundColor: '#EB9628',
    },
  },
  buttonRoot: {
    marginTop: theme.spacing(5),
    width: theme.spacing(40),
    height: theme.spacing(5),
    borderRadius: theme.spacing(20),
    background:
      'linear-gradient(90deg, #671A6C 0%, #2787FF 100%, #58A1FC 100%);',
  },
  name: {
    fontWeight: 'bold',
    color: '#EB9628',
  },
  command: {
    backgroundColor: '#242424',
    padding: `0 ${theme.spacing(1)}px`,
    borderRadius: theme.spacing(1),
  },
  commandsContainer: {
    margin: theme.spacing(2),
    width: theme.spacing(70),
    display: 'flex',
    justifyContent: 'space-between',
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative',
  },
}));

const CommandSettings = () => {
  const classes = useStyles();
  const history = useHistory();
  const keyVoice = useSelector(state => state.settings.keyVoice);

  const saveCommands = () => {
    history.goBack();
  };

  return (
    <Container maxWidth="md" className={classes.container} component="paper">
      <div className={classes.root}>
        <div className={classes.commandsContainer}>
          <Typography variant="h6">Play music</Typography>
          <div className={classes.command}>
            <Typography variant="h6">
              <span className={classes.name}>{keyVoice}</span> play
            </Typography>
          </div>
        </div>
        <div className={classes.commandsContainer}>
          <Typography variant="h6">Stop music</Typography>
          <div className={classes.command}>
            <Typography variant="h6">
              <span className={classes.name}>{keyVoice}</span> stop
            </Typography>
          </div>
        </div>
        <div className={classes.commandsContainer}>
          <Typography variant="h6">Pause music</Typography>
          <div className={classes.command}>
            <Typography variant="h6">
              <span className={classes.name}>{keyVoice}</span> pause
            </Typography>
          </div>
        </div>
        <div className={classes.commandsContainer}>
          <Typography variant="h6">Next song</Typography>
          <div className={classes.command}>
            <Typography variant="h6">
              <span className={classes.name}>{keyVoice}</span> next
            </Typography>
          </div>
        </div>
        <div className={classes.commandsContainer}>
          <Typography variant="h6">Previous song</Typography>
          <div className={classes.command}>
            <Typography variant="h6">
              <span className={classes.name}>{keyVoice}</span> previous
            </Typography>
          </div>
        </div>
        <div className={classes.commandsContainer}>
          <Typography variant="h6">Add to favorites</Typography>
          <div className={classes.command}>
            <Typography variant="h6">
              <span className={classes.name}>{keyVoice}</span> like
            </Typography>
          </div>
        </div>
      </div>
      <Button
        onClick={saveCommands}
        classes={{
          root: classes.buttonRoot,
        }}
      >
        Save commands
      </Button>
      <IconButton
        classes={{
          root: classes.floatingButton,
        }}
      >
        <AddIcon />
      </IconButton>
    </Container>
  );
};

export default CommandSettings;
