import {
  TextField,
  Button,
  FormControlLabel,
  FormControl,
  InputLabel,
  OutlinedInput,
  IconButton,
  Checkbox,
  Link,
  Typography,
  Container,
  SvgIcon,
  InputAdornment,
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { ReactComponent as GoogleIcon } from '../../../assets/google.svg';
import MusicFlyCore from '../../../assets/musicfly-core.svg';
import clsx from 'clsx';
import { useHistory } from 'react-router-dom';
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase';
import { default as firebaseInstace } from 'firebase/app';
import 'firebase/auth';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useState } from 'react';
import './index.css';

const useStyles = makeStyles((theme) => ({
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    background:
      'linear-gradient(90deg, #671A6C 0%, #2787FF 100%, #58A1FC 100%)',
  },
  button: {
    borderRadius: '40px',
    padding: '15px 0px',
  },
  socialButton: {
    margin: theme.spacing(1),
  },
  googleButton: {
    backgroundColor: 'white',
    color: 'grey',
    '&:hover': {
      backgroundColor: 'white',
    },
  },
  facebookButton: {
    backgroundColor: '#1877F2',
  },
  spaceBetweenContainer: {
    padding: '5px 10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  registerLink: {
    display: 'flex',
    justifyContent: 'center',
    padding: '10px 10px',
  },
  orContainer: {
    padding: '10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  orLine: {
    margin: '15px',
    width: '100px',
    height: '2px',
    backgroundColor: 'white',
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    height: theme.spacing(25),
  },
  logo: {
    height: theme.spacing(25),
    width:  theme.spacing(30), 
  },
  label: {
    fontSize: theme.typography.body2.fontSize,
  },
  error: {
    fontWeight: 'bold',
    color: '#ed7b7b',
  },
  margin: {
    margin: theme.spacing(1),
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  password: {
    marginTop: theme.spacing(2),
  },
}));

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const SignIn = () => {
  const classes = useStyles();
  const history = useHistory();
  const firebase = useFirebase();
  const auth = useSelector((state) => state.firebase.auth);

  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState(false);
  const [emailErrorLabel, setEmailErrorLabel] = useState('');

  const [values, setValues] = useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  useEffect(() => {
    if (isLoaded(auth) && !isEmpty(auth)) {
      history.push('/settingUp');
    }
  }, [auth, history]);

  useEffect(() => {
    if (email === '' || validateEmail(email)) {
      setEmailError(false);
      setEmailErrorLabel('');
    } else {
      setEmailError(true);
      setEmailErrorLabel('Invalid email, please provide someone@example.com');
    }
  }, [email]);

  const loginWithGoogle = () => {
    const provider = new firebaseInstace.auth.GoogleAuthProvider();
    provider.setCustomParameters({
      prompt: 'select_account',
    });

    firebase.auth().signInWithPopup(provider);
  };

  const toSignUp = () => {
    history.push('/signUp');
  };

  return (
    <Container maxWidth="xs">
      <div className={classes.logoContainer}>
        <div class="animated bounceInDown">
          <img className={classes.logo} src={MusicFlyCore} alt="musicfly logo" />
        </div>
      </div>
      <form className={classes.form} noValidate autoComplete="off">
        <TextField
          FormHelperTextProps={{
            classes: {
              root: classes.error,
            },
          }}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoFocus
          autoComplete=""
          onChange={(event) => setEmail(event.target.value)}
          error={emailError}
          helperText={emailErrorLabel}
        />
        <FormControl fullWidth variant="outlined" className={classes.password}>
          <InputLabel htmlFor="outlined-adornment-password">
            Password *
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={90}
          />
        </FormControl>
        <div className={classes.spaceBetweenContainer}>
          <FormControlLabel
            classes={{
              label: classes.label,
            }}
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Link href="#" variant="body2">
            Forgot password?
          </Link>
        </div>
        <Button
          fullWidth
          variant="contained"
          color="primary"
          // onClick={handleSubmit}
          className={clsx(classes.button, classes.submit)}
        >
          Sign In
        </Button>
        <Link
          href="#"
          variant="body2"
          className={classes.registerLink}
          onClick={toSignUp}
        >
          {"Don't have an account? Sign Up"}
        </Link>
      </form>
      <div className={classes.orContainer}>
        <div className={classes.orLine}></div>
        <Typography variant="subtitle1" display="block" gutterBottom>
          OR
        </Typography>
        <div className={classes.orLine}></div>
      </div>
      <div>
        <Button
          variant="contained"
          fullWidth
          color="secondary"
          onClick={loginWithGoogle}
          className={clsx(
            classes.button,
            classes.socialButton,
            classes.googleButton
          )}
          startIcon={<SvgIcon component={GoogleIcon} viewBox="0 0 512 512" />}
        >
          Sign In with Google
        </Button>
      </div>
    </Container>
  );
};

export default SignIn;
