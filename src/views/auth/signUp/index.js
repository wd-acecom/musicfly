import {
  TextField,
  Button,
  Link,
  Typography,
  Container,
  SvgIcon,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
  FormHelperText,
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { ReactComponent as GoogleIcon } from '../../../assets/google.svg';
import MusicFlyCore from '../../../assets/musicfly-core.svg';
import clsx from 'clsx';
import { useHistory } from 'react-router-dom';
import { useFirebase, isLoaded, isEmpty } from 'react-redux-firebase';
import { default as firebaseInstace } from 'firebase/app';
import 'firebase/auth';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { useState } from 'react';

const useStyles = makeStyles((theme) => ({
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    background:
      'linear-gradient(90deg, #671A6C 0%, #2787FF 100%, #58A1FC 100%)',
  },
  button: {
    borderRadius: '40px',
    padding: '15px 0px',
  },
  socialButton: {
    margin: theme.spacing(1),
  },
  googleButton: {
    backgroundColor: 'white',
    color: 'grey',
    '&:hover': {
      backgroundColor: 'white',
    },
  },
  facebookButton: {
    backgroundColor: '#1877F2',
  },
  spaceBetweenContainer: {
    padding: '5px 10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  registerLink: {
    display: 'flex',
    justifyContent: 'center',
    padding: '10px 10px',
  },
  orContainer: {
    padding: '10px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  orLine: {
    margin: '15px',
    width: '100px',
    height: '2px',
    backgroundColor: 'white',
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    height: theme.spacing(25),
  },
  logo: {
    height: theme.spacing(20),
  },
  label: {
    fontSize: theme.typography.body2.fontSize,
  },
  error: {
    fontWeight: 'bold',
    color: '#ed7b7b',
  },
  valid: {
    color: '#12ea69',
  },
  password: {
    marginTop: theme.spacing(2),
  },
  block: {
    display: 'block',
  },
}));

function validateEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const PasswordValidationMessage = ({ error = true, message = '' }) => {
  const classes = useStyles();

  return (
    <span className={clsx(error ? '' : classes.valid, classes.block)}>
      {error ? 'X' : '✓'} {message}
    </span>
  );
};

const SignIn = () => {
  const classes = useStyles();
  const history = useHistory();
  const firebase = useFirebase();
  const auth = useSelector((state) => state.firebase.auth);

  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState(false);
  const [emailErrorLabel, setEmailErrorLabel] = useState('');

  // eslint-disable-next-line no-unused-vars
  const [firstName, setFirstName] = useState('');
  // eslint-disable-next-line no-unused-vars
  const [lastName, setLastName] = useState('');

  const [values, setValues] = useState({
    password: '',
    repeatPassword: '',
    showPassword: false,
    showRepeatPassword: false,
  });

  const [passwordValidation, setPasswordValidation] = useState({
    minimumLenghtError: true,
    uppercaseError: true,
    lowercaseError: true,
    numericError: true,
    specialCharacterError: true,
  });

  const [passwordError, setPasswordError] = useState(false);
  const [repeatPasswordError, setRepeatPasswordError] = useState(false);

  const [showPasswordValidation, setShowPasswordValidation] = useState(false);

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  useEffect(() => {
    const validation = () => {
      let password = values.password;

      let current = {
        minimumLenghtError: password.length < 8,
        uppercaseError: password.toLowerCase() === password,
        lowercaseError: password.toUpperCase() === password,
        numericError: !/\d/.test(password),
        specialCharacterError: !/[ `!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/.test(
          password
        ),
      };

      setPasswordValidation(current);

      return Object.values(current).every((item) => item === false);
    };

    if (values.password !== '') {
      if (validation()) {
        setPasswordError(false);
      } else {
        setPasswordError(true);
      }
      setShowPasswordValidation(true);
    } else {
      setShowPasswordValidation(false);
    }

    if (values.repeatPassword !== '') {
      if (values.password !== values.repeatPassword) {
        setRepeatPasswordError(true);
      } else {
        setRepeatPasswordError(false);
      }
    }
  }, [values]);

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleClickShowRepeatPassword = () => {
    setValues({ ...values, showRepeatPassword: !values.showRepeatPassword });
  };

  const handleMouseDownRepeatPassword = (event) => {
    event.preventDefault();
  };

  useEffect(() => {
    if (isLoaded(auth) && !isEmpty(auth)) {
      history.push('/settingUp');
    }
  }, [auth, history]);

  useEffect(() => {
    if (email === '' || validateEmail(email)) {
      setEmailError(false);
      setEmailErrorLabel('');
    } else {
      setEmailError(true);
      setEmailErrorLabel('Invalid email, please provide someone@example.com');
    }
  }, [email]);

  const loginWithGoogle = () => {
    const provider = new firebaseInstace.auth.GoogleAuthProvider();
    provider.setCustomParameters({
      prompt: 'select_account',
    });

    firebase.auth().signInWithPopup(provider);
  };

  const toSignIn = () => {
    history.push('/signIn');
  };

  return (
    <Container maxWidth="xs">
      <div className={classes.logoContainer}>
        <img className={classes.logo} src={MusicFlyCore} alt="musicfly logo" />
      </div>
      <form className={classes.form} noValidate autoComplete="off">
        <TextField
          variant="outlined"
          margin="normal"
          required
          autoFocus
          fullWidth
          name="firstName"
          label="First name"
          onChange={(event) => setFirstName(event.target.value)}
          id="firstName"
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="lastName"
          label="Last name"
          onChange={(event) => setLastName(event.target.value)}
          id="lastName"
        />
        <TextField
          FormHelperTextProps={{
            classes: {
              root: classes.error,
            },
          }}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          onChange={(event) => setEmail(event.target.value)}
          error={emailError}
          helperText={emailErrorLabel}
        />
        <FormControl
          fullWidth
          variant="outlined"
          className={classes.password}
          error={passwordError}
        >
          <InputLabel htmlFor="outlined-adornment-repeat-password">
            Password *
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-repeat-password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            required
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={90}
          />
          {showPasswordValidation && (
            <FormHelperText>
              <PasswordValidationMessage
                error={passwordValidation.minimumLenghtError}
                message="At least 8 characters"
              />
              <PasswordValidationMessage
                error={passwordValidation.uppercaseError}
                message="At least 1 uppercase character"
              />
              <PasswordValidationMessage
                error={passwordValidation.lowercaseError}
                message="At least 1 lowercase character"
              />
              <PasswordValidationMessage
                error={passwordValidation.numericError}
                message="At least 1 numeric character"
              />
              <PasswordValidationMessage
                error={passwordValidation.specialCharacterError}
                message="At least 1 special character ($, @, % ...)"
              />
            </FormHelperText>
          )}
        </FormControl>
        <FormControl
          fullWidth
          variant="outlined"
          className={classes.password}
          error={repeatPasswordError}
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Repeat password *
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showRepeatPassword ? 'text' : 'password'}
            value={values.repeatPassword}
            onChange={handleChange('repeatPassword')}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowRepeatPassword}
                  onMouseDown={handleMouseDownRepeatPassword}
                  edge="end"
                >
                  {values.showRepeatPassword ? (
                    <Visibility />
                  ) : (
                    <VisibilityOff />
                  )}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={145}
          />
          {repeatPasswordError && (
            <FormHelperText className={classes.error}>
              Passwords must be match
            </FormHelperText>
          )}
        </FormControl>
        <Button
          fullWidth
          variant="contained"
          color="primary"
          className={clsx(classes.button, classes.submit)}
        >
          Sign Up
        </Button>
        <Link
          href="#"
          variant="body2"
          className={classes.registerLink}
          onClick={toSignIn}
        >
          {'Do you have an account? Sign In'}
        </Link>
      </form>
      <div className={classes.orContainer}>
        <div className={classes.orLine}></div>
        <Typography variant="subtitle1" display="block" gutterBottom>
          OR
        </Typography>
        <div className={classes.orLine}></div>
      </div>
      <div>
        <Button
          variant="contained"
          fullWidth
          color="secondary"
          onClick={loginWithGoogle}
          className={clsx(
            classes.button,
            classes.socialButton,
            classes.googleButton
          )}
          startIcon={<SvgIcon component={GoogleIcon} viewBox="0 0 512 512" />}
        >
          Sign Up with Google
        </Button>
      </div>
    </Container>
  );
};

export default SignIn;
