import { PlayListLayout } from '../../../layouts';
import './index.css';

const ID = {
  album: 'CatchMyHeadBanging',
  albumImg:
    'https://i.pinimg.com/originals/a3/c1/83/a3c183d339f8c92dae84f760d44bb146.jpg',
  artist: 'Brando',
};

function HeaderAlbum(props) {
  const { album, albumImg, artist } = props;

  return (
    <>
      <div
        className="image"
        style={{ backgroundImage: `url('${albumImg}')` }}
      />
      <div className="info">
        <h1>{album}</h1>
        <p>De {artist}</p>
      </div>
    </>
  );
}

const Playlist = () => {
  return (
    <div>
      <br></br>
      <div className="__header">
        <HeaderAlbum
          album={ID.album}
          albumImg={ID.albumImg}
          artist={ID.artist}
        />
      </div>
      <br></br>

      <PlayListLayout></PlayListLayout>
    </div>
  );
};
export default Playlist;
