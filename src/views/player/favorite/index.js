import { PlayListLayout } from '../../../layouts';
import './index.css';

const fav = {
  album: 'Favorites',
  albumImg: 'https://lineup-images.scdn.co/wrapped-2020-top100_LARGE-es.jpg',
  artist: 'Me',
};

function HeaderAlbum(props) {
  const { album, albumImg, artist } = props;

  return (
    <>
      <div
        className="image"
        style={{ backgroundImage: `url('${albumImg}')` }}
      />
      <div className="info">
        <h1>{album}</h1>
        <p>De {artist}</p>
      </div>
    </>
  );
}

const Favorite = () => {
  return (
    <div>
      <br></br>
      <div className="__header">
        <HeaderAlbum
          album={fav.album}
          albumImg={fav.albumImg}
          artist={fav.artist}
        />
      </div>
      <br></br>

      <PlayListLayout></PlayListLayout>
    </div>
  );
};

export default Favorite;
