import { makeStyles, Typography } from '@material-ui/core';
import './index';
import { useDispatch, useSelector } from 'react-redux';
import { PlayListLayout } from '../../../../layouts';

const useStyles = makeStyles((theme) => ({
    discoverHeader: {
        background:
            'linear-gradient(180deg, #333333 0%, #171717 90.69%, #171717 100%)',
        height: theme.spacing(50),
        alignItems: 'center',
        display: 'flex',
        padding: 30,
    },
    image: {
        width: theme.spacing(40),
        height: theme.spacing(40),
        marginRight: theme.spacing(5),
    },
    empty: {
        display: 'flex',
        height: `calc(100vh - ${theme.spacing(60)}px)`,
        alignItems: 'center',
        justifyContent: 'center',
    },
}));

const DiscoverTrack = () => {
    const classes = useStyles();
    const album = useSelector((state) => state.album.album);
    const dispatch = useDispatch();

    return (
        <div>
            <div className={classes.discoverHeader}>
                <div className={classes.image}>
                    <img src={album.image} alt={album.name} />
                </div>
                <div>
                    <Typography variant="h4">
                        <b>{album.name}</b>
                    </Typography>
                    <br />
                    <hr />
                    <Typography variant="h6">{album.artist}</Typography>
                </div>
            </div>
            {album.tracks ? (
                <PlayListLayout populate={true} />
            ) : (
                <div className={classes.empty}>
                    <Typography variant="h6">
                        There are not songs for this album :(
                    </Typography>
                </div>
            )}
        </div>
    );
};

export default DiscoverTrack;
