import { PlayCardItem } from '../../../layouts/recently/components/';
import SearchIcon from '@material-ui/icons/Search';
import {
  OutlinedInput,
  InputAdornment,
  makeStyles,
  Typography,
} from '@material-ui/core';
import './index';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getAlbumsSearchRequest,
  getArtistsSearchRequest,
  getTracksSearchRequest,
} from '../../../ducks/discover';

const useStyles = makeStyles((theme) => ({
  discoverHeader: {
    background:
    'linear-gradient(180deg, #333333 0%, #171717 90.69%, #171717 100%)',
    height: '200px',
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
    justifyContent: 'space-around',
  },
}));

const Discover = () => {
  const classes = useStyles();
  const [search, setSearch] = useState('');
  const albums = useSelector((state) => state.discover.albums);
  const artists = useSelector((state) => state.discover.artists);
  const tracks = useSelector((state) => state.discover.tracks);
  const dispatch = useDispatch();

  const handleSearchChange = (evt) => {
    if (evt.key !== 'Enter') {
      setSearch(evt.target.value);
    } else {
      dispatch(getAlbumsSearchRequest({ search }));
      dispatch(getArtistsSearchRequest({ search }));
      dispatch(getTracksSearchRequest({ search }));
    }
  };

  return (
    <div>
      <div className={classes.discoverHeader}>
        <Typography variant="h3">Discover</Typography>

        <OutlinedInput
          onKeyUp={(evt) => handleSearchChange(evt)}
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          }
        />
      </div>
      <div className="ContentBody">
        {albums.length > 0 && (
          <div className="Category">
            <br></br>
            <div className="CategoryTitle">
              <h2>Albums</h2>
            </div>
            <div className="CategoryItens">
              {albums.map((album) => (
                <PlayCardItem
                  key={album.name + '^&' + album.artist}
                  title={album.name}
                  imgUrl={album.image}
                  artist={album.artist}
                  isAlbum={true}
                />
              ))}
            </div>
          </div>
        )}
        {tracks.length > 0 && (
          <div className="Category">
            <br></br>
            <div className="CategoryTitle">
              <h2>Tracks</h2>
            </div>
            <div className="CategoryItens">
              {tracks.map((track) => (
                <PlayCardItem
                  key={track.name + '^&' + track.artist}
                  title={track.name}
                  description={track.artist}
                  artist={track.artist}
                  imgUrl={track.image}
                />
              ))}
            </div>
          </div>
        )}
        {artists.length > 0 && (
          <div className="Category">
            <br></br>
            <div className="CategoryTitle">
              <h2>Artists</h2>
            </div>
            <div className="CategoryItens">
              {artists.map((artist) => (
                <PlayCardItem
                  key={artist.name}
                  title={artist.name}
                  imgUrl={artist.image}
                />
              ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Discover;
