import { PlayCardItem } from '../../../layouts/recently/components/';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import './index.css';

const RecentlyPlayed = () => {
  return (
    <div>
      <div className="Header">
        <div className="ContentTitle">Recently played</div>
      </div>
      <div className="ContentBody">
        <div className="Category">
          <br></br>
          <div className="CategoryTitle">
            <h1>Recientes</h1>
            <div>
              <ArrowBackIosIcon />
              <ArrowForwardIosIcon />
            </div>
          </div>
          <div className="CategoryItens">
            <PlayCardItem
              description="Harry Styles"
              imgUrl="https://i.scdn.co/image/b2163e7456f3d618a0e2a4e32bc892d6b11ce673"
              title="Fine Line"
            ></PlayCardItem>
            <PlayCardItem
              description="Harry Styles"
              imgUrl="https://i.scdn.co/image/b2163e7456f3d618a0e2a4e32bc892d6b11ce673"
              title="Fine Line"
            ></PlayCardItem>
            <PlayCardItem
              description="Harry Styles"
              imgUrl="https://i.scdn.co/image/b2163e7456f3d618a0e2a4e32bc892d6b11ce673"
              title="Fine Line"
            ></PlayCardItem>
          </div>
        </div>
        <div className="Category">
          <br></br>
          <div className="CategoryTitle">
            <h2>Los mas repetidos</h2>
            <div>
              <ArrowBackIosIcon />
              <ArrowForwardIosIcon />
            </div>
          </div>
          <div className="CategoryItens">
            <PlayCardItem
              description="Harry Styles"
              imgUrl="https://i.scdn.co/image/b2163e7456f3d618a0e2a4e32bc892d6b11ce673"
              title="Fine Line"
            ></PlayCardItem>
            <PlayCardItem
              description="Harry Styles"
              imgUrl="https://i.scdn.co/image/b2163e7456f3d618a0e2a4e32bc892d6b11ce673"
              title="Fine Line"
            ></PlayCardItem>
            <PlayCardItem
              description="Harry Styles"
              imgUrl="https://i.scdn.co/image/b2163e7456f3d618a0e2a4e32bc892d6b11ce673"
              title="Fine Line"
            ></PlayCardItem>
          </div>
        </div>
      </div>
      <br></br>
      <br></br>
      <br></br>
    </div>
  );
};

export default RecentlyPlayed;
