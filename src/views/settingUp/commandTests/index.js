import React, { useEffect } from 'react';
import {
  Container,
  IconButton,
  makeStyles,
  Typography,
} from '@material-ui/core';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { enrrollCommants, startContinuousArtyom } from '../../../api';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: theme.spacing(60),
  },
  buttonRoot: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    background:
      'linear-gradient(90deg, #671A6C 0%, #2787FF 100%, #58A1FC 100%);',
  },
  inputContainer: {
    borderRadius: theme.spacing(5),
    backgroundColor: '#2E2E2E',
    height: theme.spacing(10),
    width: theme.spacing(80),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '24pt',
    fontWeight: 'bold',
  },
  say: {
    color: '#7A4E89',
  },
  name: {
    color: '#8C622B',
  },
}));

var commands = [
  {
    indexes: ['Hello'],
    action: function () {
      alert('Hello, how are you ?');
    },
  },
];

const CommandTests = () => {
  const classes = useStyles();
  const history = useHistory();
  const keyVoice = useSelector((state) => state.settings.keyVoice);

  const redirect = () => {
    history.push('/player');
  };
  enrrollCommants([
    {
      indexes: [`${keyVoice} start`, `${keyVoice} star`],
      action: redirect,
    },
  ]);
  useEffect(() => {
    startContinuousArtyom();
  });

  const handleNext = () => {
    history.push('/player');
  };

  return (
    <Container maxWidth="md" className={classes.container}>
      <Typography variant="h2" align="center">
        We need to verify that your commands will be understood
      </Typography>
      <div className={classes.inputContainer}>
        <span>
          <span className={classes.say}>SAY: </span>
          <span className={classes.name}>{keyVoice} </span>
          <span className={classes.command}>start </span>
        </span>
      </div>
      <IconButton
        onClick={handleNext}
        classes={{
          root: classes.buttonRoot,
        }}
      >
        <ArrowForwardIcon fontSize="large" />
      </IconButton>
    </Container>
  );
};

export default CommandTests;
