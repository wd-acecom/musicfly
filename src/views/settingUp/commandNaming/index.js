import React, { useState } from 'react';
import {
  Container,
  IconButton,
  makeStyles,
  Typography,
  OutlinedInput,
} from '@material-ui/core';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setKeyVoice } from '../../../ducks';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: theme.spacing(60),
  },
  buttonRoot: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    background:
      'linear-gradient(90deg, #671A6C 0%, #2787FF 100%, #58A1FC 100%);',
  },
  inputContainer: {
    borderRadius: theme.spacing(5),
    backgroundColor: '#2E2E2E',
  },
  input: {
    textAlign: 'center',
    fontSize: '24pt',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
}));

const CommandsNaming = () => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const [botName, setBotName] = useState('musicfly');

  const handleTextChange = (text) => {
    if (text.length <= 10) {
      setBotName(text);
    }
  };

  const handleNext = () => {
    dispatch(setKeyVoice(botName));
    history.push('/settingUp/commandTests');
  };

  return (
    <Container maxWidth="md" className={classes.container}>
      <Typography variant="h2" align="center">
        Before we start, we need a name to hear your commands
      </Typography>
      <OutlinedInput
        value={botName}
        onChange={(e) => handleTextChange(e.target.value)}
        classes={{ root: classes.inputContainer, input: classes.input }}
      />
      <IconButton
        onClick={handleNext}
        classes={{
          root: classes.buttonRoot,
        }}
      >
        <ArrowForwardIcon fontSize="large" />
      </IconButton>
    </Container>
  );
};

export default CommandsNaming;
